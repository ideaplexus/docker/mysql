ARG PROXY_CACHE_PREFIX
ARG MYSQL_VERSION=8

FROM ${PROXY_CACHE_PREFIX}mysql:${MYSQL_VERSION}

COPY custom.cnf /etc/mysql/mysql.conf.d/custom.cnf
